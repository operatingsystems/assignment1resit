/**
	* Webserver
	* Operating Systems
	* v20.11.30
	*/

/**
	Hint: Control-click on a functionname to go to the definition
	Hint: Ctrl-space to auto complete functions and variables
	*/

// function/class definitions you are going to use
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/param.h>
#include <signal.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>

#include <vector>
#include <list>
#include <map>
#include <algorithm>

// although it is good habit, you don't have to type 'std' before many objects by including this line
using namespace std;

struct Command {
	vector<string> parts = {};
};

struct Request {
	std::string method;
	std::string path;
	std::string protocol;
	std::map<std::string, std::string> headers;
};

// Parses a string to form a vector of arguments. The seperator is a space char (' ').
vector<string> splitString(const string& str, char delimiter = ' ', size_t max = std::numeric_limits<size_t>::max()) {
	vector<string> retval;
	for (size_t pos = 0; pos < str.length(); ) {
		// look for the next space
		size_t found = str.find(delimiter, pos);
		// if no space was found, this is the last word
		if (found == string::npos) {
			retval.push_back(str.substr(pos));
			break;
		}
		// filter out consequetive spaces
		if (found != pos) {
			retval.push_back(str.substr(pos, found-pos));
			if (retval.size() == max-1) {
				retval.push_back(str.substr(found+1));
				break;
			}
		}
		pos = found+1;
	}
	return retval;
}

// trim from start (in place)
static inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !std::isspace(ch);
	}));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}

// wrapper around the C execvp so it can be called with C++ strings (easier to work with)
// always start with the command itself
// always terminate with a NULL pointer
// DO NOT CHANGE THIS FUNCTION UNDER ANY CIRCUMSTANCE
int execvp(const vector<string>& args) {
	// build argument list
	const char** c_args = new const char*[args.size()+1];
	for (size_t i = 0; i < args.size(); ++i) {
		c_args[i] = args[i].c_str();
	}
	c_args[args.size()] = nullptr;
	// replace current process with new process as specified
	::execvp(c_args[0], const_cast<char**>(c_args));
	// if we got this far, there must be an error
	int retval = errno;
	// in case of failure, clean up memory
	delete[] c_args;
	return retval;
}

// Executes a command with arguments. In case of failure, returns error code.
int executeCommand(const Command& cmd) {
	auto& parts = cmd.parts;
	if (parts.size() == 0)
		return EINVAL;

	// execute external commands
	int retval = execvp(parts);
	return retval;
}

// Executes a command with arguments. In case of failure, returns error code.
bool handleRequest(Request& req, int output) {
	std::cout << "handle request on host " << req.headers["Host"] << " with path " << req.path << std::endl;

	std::string response = "HTTP/1.1 200 OK\r\nContent-Length: 6\r\n\r\nfoobar";
	write(output, response.c_str(), response.size());

	// signal that a new connection can be accepted 
	return req.headers["Connection"] != "close";
}

bool processRequest(int fd, char* buffer, size_t& length, size_t size) {
	// request header stops after an empty line (with windows line breaks)
	while (strstr(buffer, "\r\n\r\n") == NULL) {
		// if the buffer is full, abort
		if (size-length-1 == 0) {
			return false;
		}
		ssize_t s;
		do {
			s = recv(fd, &buffer[length], size-length-1, 0);
		} while (s == -1 && errno == EINTR);
		if (s <= 0) {
			// EOF (s == 0) or error (s < 0) detected
			return false;
		}
		length += s;
		buffer[length] = 0;
	}
	size_t headerLength = strstr(buffer, "\r\n\r\n") - buffer;

	// quick and dirty handling of new lines
	for (auto i = 0; i < headerLength; ++i)
		if (buffer[i] == '\r')
			buffer[i] = '\n';

	std::string header {buffer, headerLength};
	headerLength += 4;
	auto lines = splitString(header, '\n');
	if (lines.size() == 0) {
		return false;
	}
	auto firstLine = splitString(lines[0], ' ');
	if (firstLine.size() != 3) {
		return false;
	}
	Request req;
	req.method = firstLine[0];
	req.path = firstLine[1];
	req.protocol = firstLine[2];
	for (auto i = 1; i < lines.size(); ++i) {
		auto v = splitString(lines[i], ':', 2);
		if (v.size() == 2) {
			trim(v[0]);
			trim(v[1]);
			req.headers[v[0]] = v[1];
		}
	}
	bool again = handleRequest(req, fd);

	if (length - headerLength > 0) {
		memmove(buffer, &buffer[headerLength], length - headerLength);
	}
	length -= headerLength;
	buffer[length] = 0;

	return again;
}

void handleConnection(int fd) {
	char buffer[16384] = {0};
	size_t length = 0;
	while (processRequest(fd, buffer, length, sizeof(buffer)));
}

int setup(uint16_t port) {
	// mark close on exec(), so that any child processes does not inherit this socket
#ifdef __APPLE__
	int server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	fcntl(server, F_SETFD, FD_CLOEXEC);
#else
	int server = socket(AF_INET, SOCK_STREAM | SOCK_CLOEXEC, IPPROTO_TCP);
#endif
	if (server < 0) {
		perror("socket() of server socket failed");
	}

	const int one = 1;

	setsockopt(server, SOL_SOCKET, SO_REUSEADDR, static_cast<const void*>(&one), sizeof(one));

	struct sockaddr_in sin = {};
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = INADDR_ANY;
	auto rc = bind(server, reinterpret_cast<struct sockaddr*>(&sin), sizeof(sin));

	if (rc < 0) {
		perror("bind() of server socket failed (port in use?)");
	}

	rc = listen(server, 100);
	if (rc < 0) {
		perror("listen() failed");
	}

  std::cout << "listening on port " << port << std::endl;

	return server;
}

int normal() {
	int serverSocket = setup(2222);
	if (serverSocket < 0)
		return -1;
	while (true) {
		int connection = accept(serverSocket, NULL, NULL);
		std::cerr << "started connection" << std::endl;
		handleConnection(connection);
		std::cerr << "closing connection" << std::endl;
		close(connection);
	}
	return 0;
}

// framework for executing "date | tail -c 10" using raw commands
// two processes are created, and connected to each other
int step1(bool showPrompt) {
	// create communication channel shared between the two processes
	// ...

	pid_t child1 = fork();
	if (child1 == 0) {
		// redirect standard output (STDOUT_FILENO) to the input of the shared communication channel
		// free non used resources (why?)
		Command cmd = {{string("date")}};
		executeCommand(cmd);
		// display nice warning that the executable could not be found
		abort(); // if the executable is not found, we should abort. (why?)
	}

	pid_t child2 = fork();
	if (child2 == 0) {
		// redirect the output of the shared communication channel to the standard input (STDIN_FILENO).
		// free non used resources (why?)
		Command cmd = {{string("tail"), string("-c"), string("5")}};
		executeCommand(cmd);
		abort(); // if the executable is not found, we should abort. (why?)
	}

	// free non used resources (why?)
	// wait on child processes to finish (why both?)
	waitpid(child1, nullptr, 0);
	waitpid(child2, nullptr, 0);
	return 0;
}

int main(int argc, char** argv) {
	//*
	return normal();
	/*/
	return step1();
	//*/
}
